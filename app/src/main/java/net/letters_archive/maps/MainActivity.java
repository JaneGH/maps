package net.letters_archive.maps;

//keytool -list -v -keystore "/home/user/.android/debug.keystore" -storepass android

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    //
    GoogleMap m_googleMap;
    StreetViewPanorama m_StreetView;
    LatLng firstPoint;


    private ArrayList<LatLng> sTargets = new ArrayList();

//    static {
//        sTargets.add(new LatLng(48.7845659,2.4347999));
//        sTargets.add(new LatLng(48.8639602,2.2585036));
//        sTargets.add(new LatLng(48.8589507,2.2775174));//Париж
//        sTargets.add(new LatLng(48.8471383,2.4294888));
//        sTargets.add(new LatLng(48.8369683,2.4723538));
//        sTargets.add(new LatLng(48.863773,2.4315409));
//        sTargets.add(new LatLng(48.8017634,2.42244));
//        sTargets.add(new LatLng(48.8159518,2.3075067));
//        sTargets.add(new LatLng(49.443621,2.0531622));
//        sTargets.add(new LatLng(49.2857047,3.185181));
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createMapView();

        // Create and add our cities
        City city = new City(50.45034,30.46671);
        TourManager.addCity(city);
        City city2 = new City(50.43548, 30.44487);
        TourManager.addCity(city2);
        City city3 = new City(50.44005, 30.43561);
        TourManager.addCity(city3);
        City city4 = new City(50.44023, 30.44406);
        TourManager.addCity(city4);
        City city5 = new City(50.44025,30.44341);
        TourManager.addCity(city5);
        City city6 = new City(50.45309,30.46832);
        TourManager.addCity(city6);
        City city7 = new City(50.45094,30.48319);
        TourManager.addCity(city7);
//        City city8 = new City(48.8159518, 2.3075067);
//        TourManager.addCity(city8);
//        City city9 = new City(48.8159618, 2.0531622);
//        TourManager.addCity(city9);
//        City city10 = new City(48.7159518, 2.4347999);
//        TourManager.addCity(city10);
//        City city11 = new City(48.9159518, 2.4357999);
//        TourManager.addCity(city11);
//        City city12 = new City(50.9159518, 2.4957999);
//        TourManager.addCity(city12);


        // Initialize population
        Population pop = new Population(50, true);
        System.out.println("Initial distance: " + pop.getFittest().getDistance());

        // Evolve population for 100 generations
        pop = GA.evolvePopulation(pop);
        for (int i = 0; i < 100; i++) {
            pop = GA.evolvePopulation(pop);
        }

        // Print final results
        System.out.println("Finished");
        System.out.println("Final distance: " + pop.getFittest().getDistance());
        System.out.println("Solution:");
        System.out.println(pop.getFittest());

        Tour fittest = pop.getFittest();


        int j=0;
        for (int i = 0; i < fittest.tourSize(); i++) {

            if ((j==9) && (sTargets.size() == 9)) {

                LatLng origin = sTargets.get(0);
                LatLng dest = sTargets.get(1);

//             Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);
                DownloadTask downloadTask = new DownloadTask();

//             Start downloading json data from Google Directions API
                downloadTask.execute(url);

                sTargets.clear();
                i = i - 1;
                j=0;

            }
            j++;
            LatLng target = new LatLng(fittest.getCity(i).getX(),fittest.getCity(i).getY());
            sTargets.add(target);
            if (i == 0) {
                firstPoint = target;
            }

            //Creating MarkerOptions
            MarkerOptions options = new MarkerOptions();

            // Setting the position of the marker
            options.position(target);

            /**
             * For the start location, the color of marker is GREEN and
             * for the end location, the color of marker is RED and
             * for the rest of markers, the color is AZURE
             */
            if (i == 0) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//            } else if (i == 1) {
//                 options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            } else {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            }

            // Add new marker to the Google Map Android API V2
            m_googleMap.addMarker(options);

        }

        if (sTargets.size()>2) {

            LatLng origin = sTargets.get(0);
            LatLng dest = sTargets.get(1);

          //  LatLng dest = firstPoint;

            //             Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            //             Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }


        CameraPosition cameraPosition = new CameraPosition.Builder().target(firstPoint).zoom(14.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        m_googleMap.moveCamera(cameraUpdate);


    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route

        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 2; i < sTargets.size(); i++) {
            LatLng point = (LatLng) sTargets.get(i);
            if (i == 2)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints + "&mode=driving";

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

// Fetches data from url passed
private class DownloadTask extends AsyncTask<String, Void, String> {

    // Downloading data in non-ui thread
    @Override
    protected String doInBackground(String... url) {

        // For storing data from web service
        String data = "";

        try {
            // Fetching the data from web service
            data = downloadUrl(url[0]);
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    // Executes in UI thread, after the execution of
    // doInBackground()
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        ParserTask parserTask = new ParserTask();

        // Invokes the thread for parsing the JSON data
        parserTask.execute(result);

    }
}

/**
 * A class to parse the Google Places in JSON format
 */
private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    // Parsing the data in non-ui thread
    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

        JSONObject jObject;
        List<List<HashMap<String, String>>> routes = null;

        try {
            jObject = new JSONObject(jsonData[0]);
            DirectionsJSONParser parser = new DirectionsJSONParser();

            // Starts parsing data
            routes = parser.parse(jObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    // Executes in UI thread, after the parsing process
    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {

        ArrayList points = null;
        PolylineOptions lineOptions = null;

        // Traversing through all the routes
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList();
            lineOptions = new PolylineOptions();

            // Fetching i-th route
            List<HashMap<String, String>> path = result.get(i);

            // Fetching all the points in i-th route
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(5);
            lineOptions.color(Color.BLUE);
        }

        // Drawing polyline in the Google Map for the i-th route
        m_googleMap.addPolyline(lineOptions);
    }

}


    /**
     * Set up the onClickListener that will pass the selected lat/long
     * co-ordinates through to the Street View fragment for loading
     */

//        double[][] graph = new double[sTargets.size()][sTargets.size()];
//
//        for (int i = 0; i < sTargets.size(); i++) {
//            for (int j = 0; j < sTargets.size(); j++) {
//                double v = SphericalUtil.computeDistanceBetween(sTargets.get(i), sTargets.get(j));
//                graph[i][j] = v;
//            }
//        }

    //floydWarshall(graph);


//        PolylineOptions line = new PolylineOptions();
//        line.width(4f).color(R.color.colorAccent);
//        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
//        for (int i = 0; i < sTargets.size(); i++) {
//            if (i == 0) {
//                MarkerOptions startMarkerOptions = new MarkerOptions()
//                        .position(sTargets.get(i))
//                        .icon(BdistanceitmapDescriptorFactory.fromResource(R.drawable.bubble_mask));
//                m_googleMap.addMarker(startMarkerOptions);
//            } else if (i == sTargets.size() - 1) {
//                MarkerOptions endMarkerOptions = new MarkerOptions()
//                        .position(sTargets.get(i))
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.bubble_mask));
//                m_googleMap.addMarker(endMarkerOptions);
//            }
//            line.add(sTargets.get(i));
//            latLngBuilder.include(sTargets.get(i));
//        }
//        m_googleMap.addPolyline(line);
//        int size = getResources().getDisplayMetrics().widthPixels;
//        LatLngBounds latLngBounds = latLngBuilder.build();
//        CameraUpdate track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 25);
//        m_googleMap.moveCamera(track);


    /**
     * Initialises the street view member variable with the appropriate
     * fragment from the FragmentManager
     */
    private void createStreetView() {
        m_StreetView = ((StreetViewPanoramaFragment)
                getFragmentManager().findFragmentById(R.id.streetView))
                .getStreetViewPanorama();
    }

    /**
     * Initialises the mapview
     */
    private void createMapView() {
        /**
         * Catch the null pointer exception that
         * may be thrown when initialising the map
         */
        try {
            if (null == m_googleMap) {
                m_googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                        R.id.mapView)).getMap();

                if (null == m_googleMap) {
                    Toast.makeText(getApplicationContext(),
                            "Error creating map", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (NullPointerException exception) {
            Log.e("mapApp", exception.toString());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}