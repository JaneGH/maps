package net.letters_archive.maps;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

/**
 * Created by user on 21.12.15.
 */
public class City {
    //for update
    double x;
    double y;

    // Constructs a city at chosen x, y location
    public City(double x, double y){
        this.x = x;
        this.y = y;
    }

    // Gets city's x coordinate
    public double getX(){
        return this.x;
    }

    // Gets city's y coordinate
    public double getY(){
        return this.y;
    }

    // Gets the distance to given city
    public double distanceTo(City city){
        double xDistance = Math.abs(getX() - city.getX());
        double yDistance = Math.abs(getY() - city.getY());
        double distance  = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
        //double distance = SphericalUtil.computeDistanceBetween(new LatLng(getX(),getY()), new LatLng(city.getX(),city.getY()));
        return distance;
    }

    @Override
    public String toString(){
        return getX()+", "+getY();
    }
}